package com.test.amaysim;

import android.app.Application;

import com.test.amaysim.utils.AmaysimPreferenceManager;

/**
 * Created by nikka on 22/08/2017.
 */

public class AmaysimApplication  extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        AmaysimPreferenceManager.init(this);


    }
}
