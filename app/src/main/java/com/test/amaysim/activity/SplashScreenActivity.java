package com.test.amaysim.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.test.amaysim.R;
import com.test.amaysim.db.AmaysimContentProvider;
import com.test.amaysim.db.AmaysimDatabaseHelper;
import com.test.amaysim.model.Included;
import com.test.amaysim.model.MainJSON;
import com.test.amaysim.utils.AmaysimPreferenceManager;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class SplashScreenActivity extends AppCompatActivity {

    private static final String TAG = "SplashScreenActivity";

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            if(msg.what == 100){
                Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        if(AmaysimPreferenceManager.getUserId()!=null){
            handler.sendEmptyMessageDelayed(100, 5000);
        }else{
            new JSONParse().execute();
        }

        }




    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("collection.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    private class JSONParse extends AsyncTask<String, String, MainJSON> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "start json parsing");
        }

        @Override
        protected MainJSON doInBackground(String... args) {
            Log.d(TAG, "parsing...");
            Log.d(TAG, loadJSONFromAsset());
            Gson gson = new Gson();

            MainJSON mainJSON= gson.fromJson(loadJSONFromAsset(), MainJSON.class);

            AmaysimPreferenceManager.setUserId(mainJSON.getData().getId());
            AmaysimPreferenceManager.setPaymentType(mainJSON.getData().getType());
            AmaysimPreferenceManager.setEmail(mainJSON.getData().getAttributes().getEmailAddress());
            AmaysimPreferenceManager.setTitle(mainJSON.getData().getAttributes().getTitle());
            AmaysimPreferenceManager.setFirstName(mainJSON.getData().getAttributes().getFirstName());
            AmaysimPreferenceManager.setLastName(mainJSON.getData().getAttributes().getLastName());
            AmaysimPreferenceManager.setContactNumber(mainJSON.getData().getAttributes().getContactNumber());
            AmaysimPreferenceManager.setUnbilledCharged(mainJSON.getData().getAttributes().getUnbilledCharges());
            AmaysimPreferenceManager.setNextBillingDate(mainJSON.getData().getAttributes().getNextBillingDate());

            List<Included> allPackages = mainJSON.getIncluded();
            ContentValues contentValues = null;
            for (Included included : allPackages){
                contentValues = new ContentValues();
                Log.d(TAG, included.getType());
                if(included.getType().equals("services")){

                    contentValues.put(AmaysimDatabaseHelper.TABLE_PACKAGES_COLUMN_TYPE, AmaysimDatabaseHelper.TYPE_SERVICE);
                    Log.d(TAG, included.getAttributes().toString());
                    contentValues.put(AmaysimDatabaseHelper.TABLE_PACKAGES_ATTRIBUTES, gson.toJson(included.getAttributes()));
                    getContentResolver().insert(AmaysimContentProvider.CONTENT_URI_PACKAGES, contentValues);
                }else if (included.getType().equals("subscriptions")){

                    contentValues.put(AmaysimDatabaseHelper.TABLE_PACKAGES_COLUMN_TYPE, AmaysimDatabaseHelper.TYPE_SUBSCRIPTION);
                    Log.d(TAG, included.getAttributes().toString());
                    contentValues.put(AmaysimDatabaseHelper.TABLE_PACKAGES_ATTRIBUTES, gson.toJson(included.getAttributes()));
                    getContentResolver().insert(AmaysimContentProvider.CONTENT_URI_PACKAGES, contentValues);
                }else if (included.getType().equals("products")){

                    contentValues.put(AmaysimDatabaseHelper.TABLE_PACKAGES_COLUMN_TYPE, AmaysimDatabaseHelper.TYPE_PRODUCT);
                    Log.d(TAG, included.getAttributes().toString());
                    contentValues.put(AmaysimDatabaseHelper.TABLE_PACKAGES_ATTRIBUTES, gson.toJson(included.getAttributes()));
                    getContentResolver().insert(AmaysimContentProvider.CONTENT_URI_PACKAGES, contentValues);
                }

            }

            return mainJSON;
        }

        @Override
        protected void onPostExecute(MainJSON json) {

            handler.sendEmptyMessageDelayed(100, 5000);

        }
    }


}
