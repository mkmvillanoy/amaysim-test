package com.test.amaysim.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.test.amaysim.db.AmaysimContentProvider;
import com.test.amaysim.db.AmaysimDatabaseHelper;
import com.test.amaysim.R;
import com.test.amaysim.model.IncludedAttributes;
import com.test.amaysim.model.Subscriptions;

import org.w3c.dom.Text;

/**
 * Created by nikka on 22/08/2017.
 */

public class PackagesAdapter extends CursorRecyclerViewAdapter{
    private static final String TAG = "PackagesAdapter";

    private Cursor mCursor;
    private Context mContext;
    private Gson gson = new Gson();
    public PackagesAdapter(Context context, Cursor cursor) {
        super(context, cursor);
        mCursor=cursor;
        mContext = context;
        Log.d(TAG,""+cursor.getCount());

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, Cursor cursor) {

        int type = getItemViewType(cursor);

        String attributeString = cursor.getString(cursor.getColumnIndex(AmaysimDatabaseHelper.TABLE_PACKAGES_ATTRIBUTES));
        Log.d(TAG, "onBindViewHolder "+attributeString);
        IncludedAttributes attributed = gson.fromJson(attributeString, IncludedAttributes.class);
        Log.d(TAG, "onBindViewHolder "+attributed.toString());
        if(type == AmaysimDatabaseHelper.TYPE_SERVICE && viewHolder instanceof ServicesViewHolder){
            ((ServicesViewHolder)viewHolder).mCreditExpiry.setText("Credit Expiry: "+attributed.getCreditExpiry());
            ((ServicesViewHolder)viewHolder).mRemainingCredit.setText("Remaining Credit: "+(attributed.getCredit()/100));
        }

        if(type == AmaysimDatabaseHelper.TYPE_SUBSCRIPTION && viewHolder instanceof SubscriptionViewHolder){
            ((SubscriptionViewHolder)viewHolder).mDataBalance.setText("Data Balance:"+attributed.getIncludedDataBalance()/1000);
            ((SubscriptionViewHolder)viewHolder).mDataExpiry.setText("Data Expiry:"+attributed.getExpiryDate());
        }

        if(type == AmaysimDatabaseHelper.TYPE_PRODUCT && viewHolder instanceof ProductViewHolder){
            ((ProductViewHolder)viewHolder).mName.setText("Name:"+attributed.getName());
            ((ProductViewHolder)viewHolder).mPrice.setText("Price: "+(attributed.getPrice()/100));

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG,"onCreateViewHolder "+viewType);
        if(viewType==AmaysimDatabaseHelper.TYPE_SERVICE) {
            View mView = LayoutInflater.from(mContext).inflate(R.layout.services_card, parent, false);
            RecyclerView.ViewHolder vh = new ServicesViewHolder(mView);
            return vh;

        }else  if(viewType==AmaysimDatabaseHelper.TYPE_SUBSCRIPTION) {
            View mView = LayoutInflater.from(mContext).inflate(R.layout.subscription_card, parent, false);
            RecyclerView.ViewHolder vh = new SubscriptionViewHolder(mView);
            return vh;

        }else  if(viewType==AmaysimDatabaseHelper.TYPE_PRODUCT) {
            View mView = LayoutInflater.from(mContext).inflate(R.layout.products_card, parent, false);
            RecyclerView.ViewHolder vh = new ProductViewHolder(mView);
            return vh;
        }

        return null;
    }


    private int getItemViewType(Cursor cursor) {
        int type = cursor.getInt(cursor.getColumnIndex(AmaysimDatabaseHelper.TABLE_PACKAGES_COLUMN_TYPE));
        Log.d(TAG, "getItemViewType2 "+type);
        return type;
    }

    @Override
    public int getItemViewType(int position) {
        mCursor.moveToPosition(position);
        int type = mCursor.getInt(mCursor.getColumnIndex(AmaysimDatabaseHelper.TABLE_PACKAGES_COLUMN_TYPE));
        Log.d(TAG, "getItemViewType "+type+" "+position);
        return type;
    }


    class ServicesViewHolder extends RecyclerView.ViewHolder {
        TextView mRemainingCredit;
        TextView mCreditExpiry;
        TextView mDataUsage;

        public ServicesViewHolder(View view) {
            super(view);

            mRemainingCredit = (TextView) view.findViewById(R.id.remaining_credit_text_view);
            mCreditExpiry = (TextView) view.findViewById(R.id.credit_expiry_text_view);
            mDataUsage = (TextView) view.findViewById(R.id.data_usage);
        }
    }

    class SubscriptionViewHolder extends RecyclerView.ViewHolder {
        TextView mDataBalance;
        TextView mDataExpiry;

        public SubscriptionViewHolder(View view) {
            super(view);
            mDataBalance = (TextView) view.findViewById(R.id.data_balance);
            mDataExpiry = (TextView) view.findViewById(R.id.expiration_date);
        }

    }

    class ProductViewHolder extends RecyclerView.ViewHolder {
        TextView mName;
        TextView mPrice;
        TextView unlimitedTalk;
        TextView unlimitedInternationalTalk;
        TextView unlimitedText;
        TextView unlimitedInternationalText;

        public ProductViewHolder(View view) {
            super(view);
            mName = (TextView) view.findViewById(R.id.name);
            mPrice = (TextView) view.findViewById(R.id.price);
            unlimitedTalk = (TextView) view.findViewById(R.id.unlimited_talk);
            unlimitedInternationalTalk = (TextView) view.findViewById(R.id.unlimited_international_talk);
            unlimitedText = (TextView) view.findViewById(R.id.unlimited_text);
            unlimitedInternationalText = (TextView) view.findViewById(R.id.unlimited_international_text);
        }

    }


}
