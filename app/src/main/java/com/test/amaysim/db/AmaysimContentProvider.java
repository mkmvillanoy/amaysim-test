package com.test.amaysim.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;

public class AmaysimContentProvider extends ContentProvider {
	private static final String TAG = "AmaysimContentProvider";
	public static final String PROVIDER_NAME = "com.test.amaysim.provider";
	public static final Uri CONTENT_URI_PACKAGES = Uri.parse("content://"
			+ PROVIDER_NAME + "/packages");
	private static final int PACKAGE = 1;
	private static final int SINGLE_PACKAGE = 2;

	private static final UriMatcher uriMatcher;

	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(PROVIDER_NAME, "packages", PACKAGE);
		uriMatcher.addURI(PROVIDER_NAME, "packages/*", SINGLE_PACKAGE);
	}

	AmaysimDatabaseHelper mAmaysimDatabaseHelper;

	@Override
	public boolean onCreate() {
		mAmaysimDatabaseHelper = new AmaysimDatabaseHelper(getContext());
		return true;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		SQLiteDatabase db = mAmaysimDatabaseHelper.getWritableDatabase();
		String id = null;
		int type = uriMatcher.match(uri);
		switch (type) {
		case PACKAGE:
			break;
		case SINGLE_PACKAGE:
			id = uri.getPathSegments().get(1);
			selection = AmaysimDatabaseHelper.TABLE_PACKAGES_COLUMN_ID
					+ "=\""
					+ id
					+ "\""
					+ (!TextUtils.isEmpty(selection) ? " AND (" + selection
							+ ')' : "");

			break;
		default:
			break;
		}

		int deleteCount = 0;

		if (type == PACKAGE || type == SINGLE_PACKAGE) {
			deleteCount = db.delete(AmaysimDatabaseHelper.TABLE_PACKAGES, selection,
					selectionArgs);

		}
		getContext().getContentResolver().notifyChange(uri, null);
		return deleteCount;
	}

	@Override
	public String getType(Uri uri) {
		int type = uriMatcher.match(uri);
		switch (type) {
		case PACKAGE:
			return "vnd.android.cursor.dir/packages";
		case SINGLE_PACKAGE:
			return "vnd.android.cursor.item/packages";
		default:
			break;
		}
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		SQLiteDatabase db = mAmaysimDatabaseHelper.getWritableDatabase();
		int type = uriMatcher.match(uri);
		if (type == PACKAGE || type == SINGLE_PACKAGE) {
			long id = db.replace(AmaysimDatabaseHelper.TABLE_PACKAGES, null, values);
			getContext().getContentResolver().notifyChange(uri, null);

			return Uri.parse(CONTENT_URI_PACKAGES + "/" + id);
		}

		return null;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
		Cursor c = null;
		if (uriMatcher.match(uri) == PACKAGE) {
			c = mAmaysimDatabaseHelper.getAllPackages(projection, selection, selectionArgs, sortOrder, "");
			c.setNotificationUri(getContext().getContentResolver(), uri);
		} else if (uriMatcher.match(uri) == SINGLE_PACKAGE) {
			String id = uri.getPathSegments().get(1);
			c = mAmaysimDatabaseHelper.getPackage(id);
		}
		return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
		SQLiteDatabase db = mAmaysimDatabaseHelper.getWritableDatabase();
		int type = uriMatcher.match(uri);
		String id = null;
		switch (type) {
		case PACKAGE:
			break;
		case SINGLE_PACKAGE:
			id = uri.getPathSegments().get(1);
			selection = AmaysimDatabaseHelper.TABLE_PACKAGES_COLUMN_ID
					+ "=\""
					+ id
					+ "\""
					+ (!TextUtils.isEmpty(selection) ? " AND (" + selection
							+ ')' : "");
			break;
		default: 
			break;
		}

		int updateContact = 0;
		if (type == PACKAGE || type == SINGLE_PACKAGE) {
			db.updateWithOnConflict(AmaysimDatabaseHelper.TABLE_PACKAGES, values,
					selection, selectionArgs, SQLiteDatabase.CONFLICT_REPLACE);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return updateContact;
	}

	public AmaysimDatabaseHelper getDatabaseHelper() {
		return mAmaysimDatabaseHelper;
	}

}
