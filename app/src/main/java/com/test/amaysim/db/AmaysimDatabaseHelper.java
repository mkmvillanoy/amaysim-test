package com.test.amaysim.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class AmaysimDatabaseHelper extends SQLiteOpenHelper {
    // Table Names
    public static final String TABLE_PACKAGES = "amaysim_packages";
    // Paper Table Columns
    public static final String TABLE_PACKAGES_COLUMN_ID = "_id";
    public static final String TABLE_PACKAGES_COLUMN_TYPE = "type";
    public static final String TABLE_PACKAGES_ATTRIBUTES = "attributes";

    public static final String[] TABLE_PACKAGES_ALL_COLUMNS = {
            TABLE_PACKAGES_COLUMN_ID, TABLE_PACKAGES_COLUMN_TYPE,
            TABLE_PACKAGES_ATTRIBUTES};

    public static final String CREATE_TABLE_PACKAGES = "CREATE TABLE IF NOT EXISTS "
            + TABLE_PACKAGES
            + " (_id TEXT PRIMARY KEY, "
            + TABLE_PACKAGES_COLUMN_TYPE
            + " INTEGER NOT NULL, "
            + TABLE_PACKAGES_ATTRIBUTES
            + " TEXT); ";

    public static final int TYPE_SERVICE = 1;
    public static final int TYPE_SUBSCRIPTION = 2;
    public static final int TYPE_PRODUCT = 3;


    private static final String TAG = "AmaysimDatabaseHelper";

    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "amaysimDB";

    public AmaysimDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_PACKAGES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public Cursor getAllPackages(String[] projection, String selection, String[] selectionArgs, String orderBy, String groupBy) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.query(TABLE_PACKAGES, projection,
                selection, selectionArgs, groupBy,
                null, orderBy);
    }

    public Cursor getPackage(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.query(TABLE_PACKAGES, TABLE_PACKAGES_ALL_COLUMNS,
                TABLE_PACKAGES_COLUMN_ID + " = ? ", new String[]{id}, null,
                null, null);
    }

    public SQLiteDatabase getWritebleDatabase() {
        return this.getWritableDatabase();

    }
}
