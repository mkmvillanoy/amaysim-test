package com.test.amaysim.fragment;


import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.amaysim.R;
import com.test.amaysim.adapter.PackagesAdapter;
import com.test.amaysim.db.AmaysimContentProvider;
import com.test.amaysim.db.AmaysimDatabaseHelper;
import com.test.amaysim.utils.AmaysimPreferenceManager;

import org.w3c.dom.Text;


public class OverViewFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>  {
    private static final String TAG = "OverViewFragment";

    private RecyclerView packagesRv;
    private PackagesAdapter adapter;
    public OverViewFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_over_view, container, false);
//        TextView unbilledChargesTv = (TextView) view.findViewById(R.id.unbilled_charges);
//        TextView billCycleTv = (TextView) view.findViewById(R.id.billing_date);
//
//        unbilledChargesTv.setText(AmaysimPreferenceManager.getUnbilledCharges());
//        billCycleTv.setText(AmaysimPreferenceManager.getNextBillingDate());

        packagesRv = (RecyclerView) view.findViewById(R.id.packages_rv);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Cursor cursor = getActivity().getContentResolver().query(AmaysimContentProvider.CONTENT_URI_PACKAGES, null, null, null, AmaysimDatabaseHelper.TABLE_PACKAGES_COLUMN_TYPE);
        adapter = new PackagesAdapter(getActivity(), cursor);
        packagesRv.setLayoutManager(new LinearLayoutManager(getActivity()));

        packagesRv.setAdapter(adapter);


//        getLoaderManager().initLoader(1, null, this);



    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Uri uri = AmaysimContentProvider.CONTENT_URI_PACKAGES;
        return new CursorLoader(getContext(), uri, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        Log.d(TAG, cursor.getCount()+"");
        adapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }
}
