package com.test.amaysim.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AmaysimPreferenceManager {

    public static final String EMAIL = "email";


    public static final String USER_ID = "user_id";
    public static final String PAYMENT_TYPE = "payment_type";
    public static final String UNBILLED_CHARGES = "unbilled_charges";
    public static final String NEXT_BILLING_DATE = "next_billing_date";
    public static final String TITLE = "title";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String CONTACT_NUMBER = "contact_number";

    private static SharedPreferences pref;

    public static void init(final Context context) {
        pref = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void putString(final String key, final String value) {
        pref.edit().putString(key, value).apply();
    }

    public static String getString(final String key) {
        return pref.getString(key, null);
    }

    public static float getFloat(final String key) {
        return pref.getFloat(key, 0);
    }

    public static void putFloat(final String key, final float value) {
        pref.edit().putFloat(key, value).apply();
    }

    public static int getInt(final String key) {
        return pref.getInt(key, 0);
    }

    public static void putInt(final String key, final int value) {
        pref.edit().putInt(key, value).apply();
    }

    public static boolean getBoolean(final String key) {
        return pref.getBoolean(key, false);
    }

    public static void putBoolean(final String key, final boolean value) {
        pref.edit().putBoolean(key, value).apply();
    }

    public static void remove(final String key) {
        pref.edit().remove(key).apply();
    }

    public static void clear() {
        pref.edit().clear().commit();
    }

    public static String getEmail() {
        return getString(EMAIL);
    }

    public static void setEmail(final String email) {
        putString(EMAIL, email);
    }

    public static String getUserId() {
        return getString(USER_ID);
    }

    public static void setUserId(final String userId) {
        putString(USER_ID, userId);
    }

    public static String getPaymentType() {
        return getString(PAYMENT_TYPE);
    }

    public static void setPaymentType(final String paymentType) {
        putString(PAYMENT_TYPE, paymentType);
    }

    public static String getUnbilledCharges() {
        return getString(UNBILLED_CHARGES);
    }

    public static void setUnbilledCharged(final String unbilledCharged) {
        putString(UNBILLED_CHARGES, unbilledCharged);
    }

    public static String getNextBillingDate() {
        return getString(NEXT_BILLING_DATE);
    }

    public static void setNextBillingDate(final String nextBillingDate) {
        putString(NEXT_BILLING_DATE, nextBillingDate);
    }

    public static String getTitle() {
        return getString(TITLE);
    }

    public static void setTitle(final String title) {
        putString(TITLE, title);
    }

    public static String getFirstName() {
        return getString(FIRST_NAME);
    }

    public static void setFirstName(final String firstName) {
        putString(FIRST_NAME, firstName);
    }

    public static String getLastName() {
        return getString(LAST_NAME);
    }

    public static void setLastName(final String lastName) {
        putString(LAST_NAME, lastName);
    }

    public static String getContactNumber() {
        return getString(CONTACT_NUMBER);
    }

    public static void setContactNumber(final String contactNumber) {
        putString(CONTACT_NUMBER, contactNumber);
    }

}
